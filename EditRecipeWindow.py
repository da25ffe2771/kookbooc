from PySide2.QtCore import Qt
from PySide2.QtGui import QTextDocument
from PySide2.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QHBoxLayout, QGroupBox, QWidget, QTableWidget, \
    QPushButton, QGridLayout, QCheckBox, QDoubleSpinBox, QSizePolicy, QLineEdit, QPlainTextEdit, QTextBrowser, \
    QTableWidgetItem, QHeaderView, QComboBox, QLabel, QSpinBox, QCompleter

from Recipe import Recipe
from Foodstuff import Foodstuff
from Ingredient import Ingredient
from CustomTimeDelta import CustomTimeDelta
from QuantityValidatorDelegate import QuantityValidatorDelegate
from BlockedSignals import BlockedSignals


class EditRecipeWindow(QDialog):
    """
    Window to edit recipes.
    """

    _MARKDOWN_HELP = """## Markdown tips:\n
Use two line breaks to create a new paragraph.
A single line break have no effect.\n 
Use a backslash followed by \\
a line break to add a line break inside a paragraph.\n
You can use * to put a word in *emphasis* or in a **stronger emphasis**.
You can, off course, use the two types of list:
- Unordered list with "-", "*" or "+" ;
  * Eventually with sub-elements ;
2. And ordered list with "n." or "n)" ;
2. 1) Of course with sub-element too.\n
To write a [link](write/the/path/this/way)."""

    def __init__(self, parent, recipe=Recipe()):
        """Constructor of recipe edition window."""
        QDialog.__init__(self, parent)
        self.recipe = recipe
        self.setModal(True)
        self.setWindowTitle(recipe.name if recipe.name != "" else "New recipe")
        self._build_window()
        self._config_widgets()
        self._connect_signals_and_slots()

    def _add_line(self, ingredient):
        ingredientItem = QTableWidgetItem(ingredient.name)
        ingredientItem.setFlags(ingredientItem.flags() ^ Qt.ItemIsEditable)
        quantityItem = QTableWidgetItem(str(ingredient.quantity))
        unitItem = QTableWidgetItem(ingredient.unit)
        optItem = QTableWidgetItem()
        optItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        optItem.setCheckState(Qt.Checked if ingredient.opt else Qt.Unchecked)
        pb = QPushButton("-")
        pb.setFixedWidth(20)
        row = self.ingredientsTableWidget.rowCount()

        with BlockedSignals(self.ingredientsTableWidget):
            self.ingredientsTableWidget.insertRow(row)
            self.ingredientsTableWidget.setItem(row, 0, ingredientItem)
            self.ingredientsTableWidget.setItem(row, 1, quantityItem)
            self.ingredientsTableWidget.setItem(row, 2, unitItem)
            self.ingredientsTableWidget.setItem(row, 3, optItem)
            self.ingredientsTableWidget.setCellWidget(row, 4, pb)
            pb.clicked.connect(self._delete_ingredient)

    def _add_ingredient(self):
        """Add the ingredient written in self.searchBarLineEdit to
           self.ingredientTableWidget when the "+" QPushButton is pressed."""
        name = self.searchBarLineEdit.text()

        # If this foodstuff doesn't exist, create it.
        if name not in self.parent().cb.cornucopia:
            f = self.parent().open_edit_foodstuff(Foodstuff(name))
            if f:
                name = f.name
                completer = QCompleter(self.parent().cb.cornucopia)
                completer.setCaseSensitivity(Qt.CaseInsensitive)
                self.searchBarLineEdit.setCompleter(completer)
            else:
                return

        if name != "" and name not in self.recipe.ingredients:
            ingredient = Ingredient(self.parent().cb.cornucopia[name], 1, "unit")
            self.recipe.add_ingredient(ingredient)
            self._add_line(ingredient)
            self.searchBarLineEdit.setText("")

    def _build_window(self):
        """Called by the constructor to place all the elements of the window."""
        # Main window items:
        mainLayout = QGridLayout()

        self.recipeWidget = QWidget(self)
        mainLayout.addWidget(self.recipeWidget, 1, 1)
        self.instructionsGroupBox = QGroupBox("Instructions:", self)
        mainLayout.addWidget(self.instructionsGroupBox, 1, 2)
        self.dialogButtonBox = QDialogButtonBox(QDialogButtonBox.Ok |
                                                QDialogButtonBox.Cancel)
        mainLayout.addWidget(self.dialogButtonBox, 2, 1, 1, 2)

        self.setLayout(mainLayout)

        # Recipe widget:
        recipeWidgetLayout = QVBoxLayout()

        self.nameGroupBox = QGroupBox("Name:", self.recipeWidget)
        recipeWidgetLayout.addWidget(self.nameGroupBox)
        self.typeGroupBox = QGroupBox("Type:", self.recipeWidget)
        recipeWidgetLayout.addWidget(self.typeGroupBox)
        self.timeGroupBox = QGroupBox("Time:", self.recipeWidget)
        recipeWidgetLayout.addWidget(self.timeGroupBox)
        self.ingredientsGroupBox = QGroupBox("Ingredients:", self.recipeWidget)
        recipeWidgetLayout.addWidget(self.ingredientsGroupBox)

        self.recipeWidget.setLayout(recipeWidgetLayout)

        # Name widget:
        nameGroupBoxLayout = QVBoxLayout()

        self.nameLineEdit = QLineEdit(self.nameGroupBox)
        nameGroupBoxLayout.addWidget(self.nameLineEdit)
        self.masteredCheckBox = QCheckBox("★ Mastered !", self.nameGroupBox)
        nameGroupBoxLayout.addWidget(self.masteredCheckBox)

        self.nameGroupBox.setLayout(nameGroupBoxLayout)

        # Type group box:
        typeLayout = QGridLayout()

        self.entreesCheckBox = QCheckBox("Entrée", self.typeGroupBox)
        typeLayout.addWidget(self.entreesCheckBox, 1, 1)
        self.mainCoursesCheckBox = QCheckBox("Main course", self.typeGroupBox)
        typeLayout.addWidget(self.mainCoursesCheckBox, 1, 2)
        self.dessertsCheckBox = QCheckBox("Dessert", self.typeGroupBox)
        typeLayout.addWidget(self.dessertsCheckBox, 1, 3)
        self.aperitifsCheckBox = QCheckBox("Apéritif", self.typeGroupBox)
        typeLayout.addWidget(self.aperitifsCheckBox, 2, 1)
        self.snacksCheckBox = QCheckBox("Snack", self.typeGroupBox)
        typeLayout.addWidget(self.snacksCheckBox, 2, 2)
        self.drinksCheckBox = QCheckBox("Drink", self.typeGroupBox)
        typeLayout.addWidget(self.drinksCheckBox, 2, 3)

        self.typeGroupBox.setLayout(typeLayout)

        # Time group box:
        timeLayout = QGridLayout()

        self.preparationLabel = QLabel("Preparation:", self.timeGroupBox)
        timeLayout.addWidget(self.preparationLabel, 1, 1)
        self.preparationSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.preparationSpinBox, 1, 2)
        self.preparationComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.preparationComboBox, 1, 3)

        self.awaitingLabel = QLabel("Awaiting:", self.timeGroupBox)
        timeLayout.addWidget(self.awaitingLabel, 2, 1)
        self.awaitingSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.awaitingSpinBox, 2, 2)
        self.awaitingComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.awaitingComboBox, 2, 3)

        self.cookingLabel = QLabel("Cooking:", self.timeGroupBox)
        timeLayout.addWidget(self.cookingLabel, 3, 1)
        self.cookingSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.cookingSpinBox, 3, 2)
        self.cookingComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.cookingComboBox, 3, 3)

        self.totalTimeLabel = QLabel("Total time:", self.timeGroupBox)
        timeLayout.addWidget(self.totalTimeLabel, 4, 1)
        self.totalTimeLineEdit = QLineEdit(self.timeGroupBox)
        timeLayout.addWidget(self.totalTimeLineEdit, 4, 2, 1, 2)

        self.timeGroupBox.setLayout(timeLayout)

        # Ingredients group box:
        ingredientsGroupBoxLayout = QGridLayout()

        ingredientsGroupBoxLayout.addWidget(
            QLabel("For ", self.ingredientsGroupBox), 1, 1)
        self.guestsSpinBox = QSpinBox(self.ingredientsGroupBox)
        ingredientsGroupBoxLayout.addWidget(self.guestsSpinBox, 1, 2)
        self.guestLabel = QLabel(" guest:", self.ingredientsGroupBox)
        ingredientsGroupBoxLayout.addWidget(self.guestLabel, 1, 3)

        self.searchBarLineEdit = QLineEdit(self.ingredientsGroupBox)
        ingredientsGroupBoxLayout.addWidget(self.searchBarLineEdit, 2, 1, 1, 3)
        self.addIngredientPushButton = QPushButton("+", self.ingredientsGroupBox)

        ingredientsGroupBoxLayout.addWidget(self.addIngredientPushButton, 2,4)
        self.ingredientsTableWidget = QTableWidget(0, 5, self.ingredientsGroupBox)
        ingredientsGroupBoxLayout.addWidget(self.ingredientsTableWidget, 3, 1,
                                            1, 4)

        self.ingredientsGroupBox.setLayout(ingredientsGroupBoxLayout)

        # Instructions group box:
        instructionGroupBoxLayout = QVBoxLayout()

        self.instructionsTextEdit = QPlainTextEdit(self.instructionsGroupBox)
        instructionGroupBoxLayout.addWidget(self.instructionsTextEdit)
        self.instructionsTextBrowser = QTextBrowser(self)
        instructionGroupBoxLayout.addWidget(self.instructionsTextBrowser)

        self.instructionsGroupBox.setLayout(instructionGroupBoxLayout)

    def _config_widgets(self):
        self.recipeWidget.setSizePolicy(QSizePolicy.Fixed,
                                        QSizePolicy.Preferred)
        self.instructionsGroupBox.setSizePolicy(QSizePolicy.MinimumExpanding,
                                                QSizePolicy.Preferred)
        self.ingredientsGroupBox.setSizePolicy(QSizePolicy.Fixed,
                                               QSizePolicy.MinimumExpanding)

        self.nameLineEdit.setPlaceholderText("New recipe")
        if self.recipe.name != "": self.nameLineEdit.setText(self.recipe.name)
        elif self.parent().searchBarLineEdit.text() != "":
            self.nameLineEdit.setText(self.parent().searchBarLineEdit.text())

        self.masteredCheckBox.setChecked(self.recipe.mastered)

        for child, key in zip(self.typeGroupBox.findChildren(QCheckBox),
                               self.recipe.type):
            child.setChecked(self.recipe.type[key])

        for child in self.timeGroupBox.findChildren(QDoubleSpinBox):
            child.setDecimals(1)

        for child in self.timeGroupBox.findChildren(QComboBox):
            child.addItems(
                ("s.", "min.", "h.", "days", "weeks", "months", "years"))
        self.totalTimeLineEdit.setReadOnly(True)
        self.totalTimeLineEdit.setStyleSheet("background-color: WhiteSmoke;")

        self.preparationSpinBox.setValue(self.recipe.time["preparation"].value)
        self.preparationComboBox.setCurrentIndex(self.recipe.time["preparation"].index())
        self.awaitingSpinBox.setValue(self.recipe.time["awaiting"].value)
        self.awaitingComboBox.setCurrentIndex(self.recipe.time["awaiting"].index())
        self.cookingSpinBox.setValue(self.recipe.time["cooking"].value)
        self.cookingComboBox.setCurrentIndex(self.recipe.time["cooking"].index())
        self._update_times()

        self.guestsSpinBox.setMinimum(1)
        self.guestsSpinBox.setValue(self.recipe.guestsNumber)
        self._update_guest()

        self.searchBarLineEdit.setClearButtonEnabled(True)
        self.searchBarLineEdit.setPlaceholderText("Search Ingredient…")
        completer = QCompleter(self.parent().cb.cornucopia)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.searchBarLineEdit.setCompleter(completer)

        self.ingredientsTableWidget.setHorizontalHeaderLabels(("Ingredient", "qt.", "unit", "opt.", ""))
        self.ingredientsTableWidget.verticalHeader().hide()
        self.ingredientsTableWidget.setItemDelegateForColumn(1, QuantityValidatorDelegate())
        for ingredient in self.recipe.ingredients.values():
            self._add_line(ingredient)
        self.ingredientsTableWidget.setSizePolicy(QSizePolicy.Minimum,
                                                  QSizePolicy.MinimumExpanding)
        header = self.ingredientsTableWidget.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        header.setSectionResizeMode(0, QHeaderView.Stretch)

        self.instructionsGroupBox.setMinimumSize(600, 0)
        self.instructionsTextBrowser.setStyleSheet(
            "background-color: WhiteSmoke;")
        self.instructionsTextEdit.setPlaceholderText(self._MARKDOWN_HELP)
        self.instructionsTextEdit.setPlainText(self.recipe.instructions)
        self._update_instructions()

    def _connect_signals_and_slots(self):

        self.nameLineEdit.textEdited.connect(self._update_name)
        self.masteredCheckBox.stateChanged.connect(self._update_mastered)

        for item in self.typeGroupBox.findChildren(QCheckBox):
            item.stateChanged.connect(self._update_types)

        for item in self.timeGroupBox.findChildren(QDoubleSpinBox):
            item.valueChanged.connect(self._update_times)
        for item in self.timeGroupBox.findChildren(QComboBox):
            item.currentIndexChanged.connect(self._update_times)

        self.guestsSpinBox.valueChanged.connect(self._update_guest)
        #self.searchBarLineEdit.textChanged.connect(self._filter_ingredients)
        self.addIngredientPushButton.clicked.connect(self._add_ingredient)
        self.instructionsTextEdit.textChanged.connect(self._update_instructions)
        self.ingredientsTableWidget.itemChanged.connect(self._update_ingredients)
        self.ingredientsTableWidget.doubleClicked.connect(self._edit_ingredient)
        self.dialogButtonBox.accepted.connect(self.accept)
        self.dialogButtonBox.rejected.connect(self.reject)

    # def _filter_ingredients(self):
    #     for i in range(self.ingredientsTableWidget.rowCount()):
    #         self.ingredientsTableWidget.hideRow(i)
    #
    #     for item in self.ingredientsTableWidget.findItems(
    #             self.searchBarLineEdit.text(), Qt.MatchRegularExpression):
    #         self.ingredientsTableWidget.showRow(item.row())
    #
    #     for item in self.ingredientsTableWidget.findItems(
    #             self.searchBarLineEdit.text(), Qt.MatchContains):
    #         self.ingredientsTableWidget.showRow(item.row())

    def _edit_ingredient(self):
        if self.ingredientsTableWidget.currentColumn() != 0:
            return

        key = self.ingredientsTableWidget.currentItem().text()
        self.parent().open_edit_foodstuff(self.parent().cb.cornucopia[key])

    def _delete_ingredient(self):
        row = self.ingredientsTableWidget.currentIndex().row()
        key = self.ingredientsTableWidget.item(row, 0).text()
        self.recipe.ingredients.pop(key)
        self.ingredientsTableWidget.removeRow(row)

    def _update_ingredients(self):
        """Update recipe object when an ingredient is added or modified."""
        for row in range(self.ingredientsTableWidget.rowCount()):
            key = self.ingredientsTableWidget.item(row, 0).text()
            foodstuff = self.parent().cb.cornucopia[key]
            qt = self.ingredientsTableWidget.item(row, 1).text()
            qt = float(qt.replace(",", "."))
            unit = self.ingredientsTableWidget.item(row, 2).text()
            opt = bool(self.ingredientsTableWidget.item(row, 3).checkState())
            self.recipe.ingredients[key] = Ingredient(foodstuff, qt, unit, opt)

    def _update_instructions(self):
        """Format and display markdown text from self.instructionsTextEdit in
        self.instructionTextBrowser.
            :return: None.
        """
        self.recipe.instructions = self.instructionsTextEdit.toPlainText()
        if self.recipe.instructions != "":
            self.instructionsTextBrowser.setMarkdown(self.recipe.instructions)
        else:
            self.instructionsTextBrowser.setMarkdown(self._MARKDOWN_HELP)

    def _update_guest(self):
        self.recipe.guestsNumber = self.guestsSpinBox.value()
        if self.guestsSpinBox.value() == 1:
            self.guestLabel.setText(" guest:")
        else:
            self.guestLabel.setText(" guests:")

    def _update_mastered(self):
        self.recipe.mastered = self.masteredCheckBox.isChecked()

    def _update_times(self):
        """
        Update recipe object and display total time in totalTimeLineEdit when
        one of the time widget is edited.
        :return: None
        """
        for spinbox, combobox, key in zip(self.timeGroupBox.findChildren(QDoubleSpinBox),
                                          self.timeGroupBox.findChildren(QComboBox),
                                          self.recipe.time.keys()):
            self.recipe.time[key] = CustomTimeDelta(value=spinbox.value(),
                                                    unit=combobox.currentText())
        self.totalTimeLineEdit.setText(
            self.recipe.total_time().approximate_str())

    def _update_types(self):
        for child, key in zip(self.typeGroupBox.findChildren(QCheckBox),
                              self.recipe.type):
            self.recipe.type[key] = bool(child.checkState())



    def _update_name(self):
        self.recipe.name = self.nameLineEdit.text()
        self.setWindowTitle(
            self.recipe.name if self.recipe.name != "" else "New recipe")

    def run(self):
        """Show the recipe edition window and start its event loop.

            :return: [Recipe] object on "Ok" button or None on "Cancel".
        """
        self.show()
        if self.exec_():
#            self._parse_recipe()
            return self.recipe
        else:
            return None
