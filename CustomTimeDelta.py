from datetime import timedelta


class CustomTimeDelta(timedelta):
    """
    A timedelta object customised to deal with units and approximated values.
    """
    SECOND = {"second", "seconds", "sec.", "sec", "s.", "s"}
    MINUTE = {"minute", "minutes", "min.", "min", "m.", "m"}
    HOUR = {"hour", "hours", "h.", "h"}
    DAY = {"day", "days", "d.", "d"}
    WEEK = {"week", "weeks", "w.", "w"}
    MONTH = {"month", "months", "mon.", "mon", "M.", "M"}
    YEAR = {"year", "years", "y.", "y"}

    UNITS = dict(second=SECOND, minute=MINUTE, hour=HOUR, day=DAY,
                 week=WEEK, month=MONTH, year=YEAR)

    DAYS_BY_YEAR = 365.2425
    DAYS_BY_MONTH = DAYS_BY_YEAR / 12

    def __new__(cls, seconds=0., minutes=0., hours=0., days=0., weeks=0.,
                months=0., years=0., value=0., unit=""):
        if unit in cls.SECOND:
            seconds += value
        elif unit in cls.MINUTE:
            minutes += value
        elif unit in cls.HOUR:
            hours += value
        elif unit in cls.DAY:
            days += value
        elif unit in cls.WEEK:
            weeks += value
        elif unit in cls.MONTH:
            months += value
        elif unit in cls.YEAR:
            years += value
        months += years*12
        days += months * cls.DAYS_BY_MONTH
        return super().__new__(cls, seconds=seconds, minutes=minutes,
                               hours=hours, days=days, weeks=weeks)

    def __init__(self, seconds=0, minutes=0, hours=0, days=0, weeks=0, months=0,
                 years=0, value=0, unit=""):
        """
        :param seconds: number of seconds.
        :param minutes: number of minutes.
        :param hours: number of hours.
        :param days: number of days.
        :param weeks: number of weeks
        :param months: number of months
        :param years: number of years
        :param value: number of units. Not taken in account if unit is invalid.
        :param unit: unit of value.
        """
        super().__init__()
        if unit in self.SECOND:
            self._unit = "second"
        elif unit in self.MINUTE:
            self._unit = "minute"
        elif unit in self.HOUR:
            self._unit = "hour"
        elif unit in self.DAY:
            self._unit = "day"
        elif unit in self.WEEK:
            self._unit = "week"
        elif unit in self.MONTH:
            self._unit = "month"
        elif unit in self.YEAR:
            self._unit = "year"
        else:
            units = {"years": years, "months": months, "weeks": weeks, "days": days,
                     "hours": hours, "minutes": minutes, "seconds": seconds}
            for unit, value in units.items():
                if value != 0:
                    self._unit = unit
                    break
            else:
                self._unit = self.best_unit()

    def __add__(self, other):
        return CustomTimeDelta(seconds=super().__add__(other).total_seconds(),
                               unit=self._unit)

    def __radd__(self, other):
        return CustomTimeDelta(seconds=super().__radd__(other).total_seconds(),
                               unit=self._unit)

    def approximate_str(self, decimals=1):
        """
        :param decimals: number of decimals to round the value.
        :return: time in an human-readable string.
        """
        unit = self.best_unit(decimals)
        t = round(self.to(unit), decimals)
        if t > 1:
            unit += "s"
        return "{t:.{d}f} {u}".format(t=t, u=unit, d=decimals)

    def best_unit(self, decimal=1):
        """
        Return the most appropriated unit for time display. For exemple,
        it looks absurd to display the time in hh:mm:ss format if you want to
        let something fermented two months. Reciprocally,  displaying year and
        months, is over-informative, if you just want to put something in the
        oven a half-hour.
        :param decimal:
        :return:
        """
        for unit in reversed(self.UNITS):
            t = round(self.to(unit), decimal)
            if t >= 1:
                return unit
        else:  # If the delta is 0 minutes.
            return "minute"

    def index(self):
        for i, unit in enumerate(self.UNITS):
            if self._unit in unit:
                return i

    def to_seconds(self):
        return self.total_seconds()

    def to_minutes(self):
        return self.to_seconds() / 60

    def to_hours(self):
        return self.to_minutes() / 60

    def to_days(self):
        return self.to_hours() / 24

    def to_weeks(self):
        return self.to_days() / 7

    def to_months(self):
        return self.to_days() / self.DAYS_BY_MONTH

    def to_years(self):
        return self.to_days() / self.DAYS_BY_YEAR

    def to(self, unit: str):
        if unit in self.SECOND:
            return self.to_seconds()
        elif unit in self.MINUTE:
            return self.to_minutes()
        elif unit in self.HOUR:
            return self.to_hours()
        elif unit in self.DAY:
            return self.to_days()
        elif unit in self.WEEK:
            return self.to_weeks()
        elif unit in self.MONTH:
            return self.to_months()
        elif unit in self.YEAR:
            return self.to_years()

    @property
    def unit(self):
        if self.value > 1:
            return self._unit + "s"
        else:
            return self._unit

    @property
    def value(self):
        return self.to(self._unit)
