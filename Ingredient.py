from Foodstuff import Foodstuff
from lxml.etree import Element
import json


class Ingredient:
    """
    Ingredient class:
    =================

    Define an ingredient of a recipe, with its quantity and unit. Do not confuse
    with Foodstuff class which define an ingredient independently of any
    recipe with its inner characteristics, like allergens or seasons.

    .. seealso:: Foodstuff, Recipe
    """
    def __init__(self, foodstuff: Foodstuff, quantity: float, unit: str,
                 opt: bool = False):
        """
        :param foodstuff: the foodstuff used as an ingredient of a recipe.
        :param quantity: the quantity of foodstuff for the recipe.
        :param unit: the unit for the quantity of foodstuff.
        :param opt: if true the ingredient is considered as optional.
        """
        self.foodstuff = foodstuff
        self.quantity = quantity
        self.unit = unit
        self.opt = opt

    @property
    def name(self):
        """Ingredient's name which is the same as foodstuff's name."""
        return self.foodstuff.name

    @classmethod
    def from_xml(cls, ingredientElement: Element, cornutopia: dict):
        """
        Create an ingredient object from a parsed xml tree.
        The ingredient element must respect the CookBook's XMLSCHEMA.

        :param ingredientElement: Xml element describing the ingredient.
        :param cornutopia: Dictionary of all available foodstuffs.
        :return: Ingredient, as described in ingredientElement

        .. seealso:: CookBook()
        """
        return Ingredient(cornutopia[ingredientElement.get("name")],
                          float(ingredientElement.get("quantity")),
                          ingredientElement.get("unit"),
                          ingredientElement.get("opt") == "true")

    def to_xml(self):
        """
        Convert the ingredient into xml with respect to Cookbook's XMLSCHEMA.
        :return: lxml.etree.Element
        """
        return Element("ingredient", {"name": self.name,
                                      "quantity": str(self.quantity),
                                      "unit": self.unit,
                                      "opt": str(self.opt).lower()})

