from CustomTimeDelta import CustomTimeDelta


class Times:

    def __init__(self, prep={"time": 0, "unit": "min."},
                 awaiting={"time": 0, "unit": "min."},
                 cooking={"time": 0, "unit": "min."}):
        self.preparation = CustomTimeDelta(value=prep["time"],
                                           unit=prep["unit"])
        self.awaiting = CustomTimeDelta(value=awaiting["time"],
                                        unit=awaiting["unit"])
        self.cooking = CustomTimeDelta(value=cooking["time"],
                                       unit=cooking["unit"])

    @property
    def total_time(self):
        return self.preparation + self.awaiting + self.cooking

