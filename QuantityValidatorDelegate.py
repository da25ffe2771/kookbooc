from PySide2.QtWidgets import QStyledItemDelegate, QLineEdit
from PySide2.QtGui import QDoubleValidator


class QuantityValidatorDelegate(QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__(parent)

    def createEditor(self, parent, option, index):
        item = super().createEditor(parent, option, index)
        item.setValidator(QDoubleValidator(0, 1000, 1))
        return item
