#!venv/bin python
import sys
from MainWindow import MainWindow
from PySide2.QtWidgets import QApplication

if __name__ == '__main__':
    qt_app = QApplication(sys.argv)
    MainWindow().run(qt_app)


