from lxml import etree


class Foodstuff:
    """
    A foodstuff is an ingredient available in the cornucopia for all recipes of
    the program (The word “ingredient” is used for a foodstuff when it is used
    in a recipe).
    """
    MONTHSLIST = ["Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.",
                  "Sep.", "Oct.", "Nov.", "Dec."]

    def __init__(self, name="", isVegetarian=False, isVegetalian=False,
                 seasons=MONTHSLIST, allergens=set()):
        self.name = str(name)
        self.isVegetarian = bool(isVegetarian)
        self.isVegetalian = bool(isVegetalian)
        self.allergensSet = set(allergens)
        self.seasons = set(seasons)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = str(name).lower()

    @property
    def isVegetarian(self):
        return self._isVegetarian

    @isVegetarian.setter
    def isVegetarian(self, isVegetarian):
        self._isVegetarian = bool(isVegetarian)

    @property
    def isVegetalian(self):
        return self._isVegetalian

    @isVegetalian.setter
    def isVegetalian(self, isVegetalian):
        self._isVegetalian = bool(isVegetalian)

    @property
    def allergensSet(self):
        return self._allergensSet

    @allergensSet.setter
    def allergensSet(self, allergensSet):
        self._allergensSet = set(a.lower() for a in allergensSet)

    @property
    def seasons(self):
        return self._seasons

    @seasons.setter
    def seasons(self, seasons):
        self._seasons = set(seasons)

    def add_allergen(self, allergen):
        self._allergensSet.add(allergen.lower())

    @classmethod
    def from_xml(cls, foodstuffElement):
        foodstuff = Foodstuff()
        foodstuff.name = foodstuffElement.get("name")
        foodstuff.isVegetarian = foodstuffElement.get("isVegetarian") == "true"
        foodstuff.isVegetalian = foodstuffElement.get("isVegetalian") == "true"
        foodstuff.seasons = set(foodstuffElement.find("seasons").attrib)
        for allergen in foodstuffElement.findall("allergens/allergen"):
            foodstuff.allergensSet.add(allergen.get("name"))

        return foodstuff

    def to_xml(self):
        foodstuffElement = etree.Element(
            "foodstuff",
            {"name": self.name,
             "isVegetarian": str(self.isVegetarian).lower(),
             "isVegetalian": str(self.isVegetalian).lower()})
        etree.SubElement(foodstuffElement, "seasons",
                         {season: "true" for season in self.seasons})

        allergensElement = etree.SubElement(foodstuffElement, "allergens")
        for allergen in self.allergensSet:
            etree.SubElement(allergensElement, "allergen", {"name": allergen})

        return foodstuffElement
