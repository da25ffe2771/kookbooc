from collections import deque
from datetime import datetime
from lxml.etree import ElementTree
from path import Path
from PySide2.QtCore import Qt, QRegularExpression
from PySide2.QtWidgets import QAbstractItemView, QCheckBox, QComboBox, \
    QDoubleSpinBox, QFileDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, \
    QLineEdit, QListWidget, QMainWindow, QMenuBar, QMessageBox, QPushButton, \
    QRadioButton, QSizePolicy, QSpinBox, QTabBar, QTabWidget, QTextBrowser, \
    QVBoxLayout, QWidget, QTableWidget, QTableWidgetItem
import sys

from BlockedSignals import BlockedSignals
from CookBook import CookBook
from CustomTimeDelta import CustomTimeDelta
from EditFoodstuffWindow import EditFoodstuffWindow
from EditRecipeWindow import EditRecipeWindow
from Foodstuff import Foodstuff
from Recipe import Recipe


class MainWindow(QMainWindow):
    def __init__(self, cookbook=CookBook()):
        """
        Constructor of the main interface of the program.
        :param cookbook: the cookbook to be used by the program.
        """
        QMainWindow.__init__(self)
        self.showMaximized()
        self.setWindowTitle("KookbooC")
        self._build_window()
        self._config_widgets()
        self._connect_signals_and_slots()
        self.cb = cookbook
        self.cbCache = CookBook(self.cb)
        self.cbHistory = deque([], maxlen=15)
        self.cbRHistory = deque([], maxlen=15)
        self.cb.hasBeenSaved = True

    def _build_window(self):
        """Create all window widgets and set their layout."""

        # Menu bar: ————————————————————————————————————————————————————————————
        menuBar = QMenuBar()
        self.setMenuBar(menuBar)
        cookbookMenu = menuBar.addMenu("Cookbook")
        cookbookMenu.setToolTipsVisible(True)
        self.newAction = cookbookMenu.addAction("New")
        self.openAction = cookbookMenu.addAction("Open…")
        self.undoAction = cookbookMenu.addAction("Undo")
        self.redoAction = cookbookMenu.addAction("Redo")
        self.saveAction = cookbookMenu.addAction("Save")
        self.saveAsAction = cookbookMenu.addAction("Save as…")
        self.importCookbookAction = cookbookMenu.addAction("Import…")
        recipeMenu = menuBar.addMenu("Recipe")
        recipeMenu.setToolTipsVisible(True)
        self.exportAction = recipeMenu.addAction("Export…")
        self.importAction = recipeMenu.addAction("Import…")
        # ——————————————————————————————————————————————————————————————————————

        self.setCentralWidget(QWidget())

        # Main interface: ——————————————————————————————————————————————————————
        mainLayout = QHBoxLayout()

        self.filtersGroupBox = QGroupBox("Filters:", self.centralWidget())
        mainLayout.addWidget(self.filtersGroupBox)
        self.recipesWidget = QWidget(self.centralWidget())
        mainLayout.addWidget(self.recipesWidget)
        self.displayWidget = QWidget(self.centralWidget())
        mainLayout.addWidget(self.displayWidget)

        self.centralWidget().setLayout(mainLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Display pane widget: —————————————————————————————————————————————————
        displayLayout = QGridLayout()

        self.recipesTabWidget = QTabWidget(self.displayWidget)
        displayLayout.addWidget(self.recipesTabWidget, 1, 1, 1, 3)
        self.guestsLabel = QLabel("Guests number: ", self.displayWidget)
        displayLayout.addWidget(self.guestsLabel, 2, 2, 1, 1)
        self.guestsSpinBox = QSpinBox(self.displayWidget)
        displayLayout.addWidget(self.guestsSpinBox, 2, 3, 1, 1)

        self.displayWidget.setLayout(displayLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Filters pane group box: ——————————————————————————————————————————————
        filtersLayout = QVBoxLayout()

        self.masteredCheckBox = QCheckBox("★ Mastered !", self.filtersGroupBox)
        filtersLayout.addWidget(self.masteredCheckBox)
        self.typeGroupBox = QGroupBox("Type:", self.filtersGroupBox)
        filtersLayout.addWidget(self.typeGroupBox)
        self.timeGroupBox = QGroupBox("Maximum time:", self.filtersGroupBox)
        filtersLayout.addWidget(self.timeGroupBox)
        self.cornucopiaGroupBox =QGroupBox("Ingredients:",
                                           self.filtersGroupBox)
        filtersLayout.addWidget(self.cornucopiaGroupBox)

        self.filtersGroupBox.setLayout(filtersLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Recipes pane widget: —————————————————————————————————————————————————
        recipesLayout = QVBoxLayout()

        self.searchBarLineEdit = QLineEdit(self.recipesWidget)
        recipesLayout.addWidget(self.searchBarLineEdit)
        self.recipesListView = QListWidget(self.recipesWidget)
        recipesLayout.addWidget(self.recipesListView)
        self.recipesButtons = QWidget(self.recipesWidget)
        recipesLayout.addWidget(self.recipesButtons)

        self.recipesWidget.setLayout(recipesLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Recipes buttons widget in recipes pane: ——————————————————————————————
        buttonsLayout = QHBoxLayout()

        self.addRecipePushButton = QPushButton("Add…", self.recipesButtons)
        buttonsLayout.addWidget(self.addRecipePushButton)
        self.editRecipePushButton = QPushButton("Improve…", self.recipesButtons)
        buttonsLayout.addWidget(self.editRecipePushButton)
        self.deleteRecipePushButton = QPushButton("Forget", self.recipesButtons)
        buttonsLayout.addWidget(self.deleteRecipePushButton)

        self.recipesButtons.setLayout(buttonsLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Type group box in filters pane: ——————————————————————————————————————
        typeLayout = QGridLayout()

        self.entreesCheckBox = QCheckBox("Entrées", self.typeGroupBox)
        typeLayout.addWidget(self.entreesCheckBox, 2, 1)
        self.mainCoursesCheckBox = QCheckBox("Main courses", self.typeGroupBox)
        typeLayout.addWidget(self.mainCoursesCheckBox, 2, 2)
        self.dessertsCheckBox = QCheckBox("Desserts", self.typeGroupBox)
        typeLayout.addWidget(self.dessertsCheckBox, 2, 3)
        self.aperitifsCheckBox = QCheckBox("Apéritifs", self.typeGroupBox)
        typeLayout.addWidget(self.aperitifsCheckBox, 3, 1)
        self.snacksCheckBox = QCheckBox("Snacks", self.typeGroupBox)
        typeLayout.addWidget(self.snacksCheckBox, 3, 2)
        self.drinksCheckBox = QCheckBox("Drinks", self.typeGroupBox)
        typeLayout.addWidget(self.drinksCheckBox, 3, 3)

        self.typeGroupBox.setLayout(typeLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Time group box in filters pane: ——————————————————————————————————————
        timeLayout = QGridLayout()

        self.preparationCheckBox = QCheckBox("Preparation:", self.timeGroupBox)
        timeLayout.addWidget(self.preparationCheckBox, 1, 1)
        self.preparationSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.preparationSpinBox, 1, 2)
        self.preparationComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.preparationComboBox, 1, 3)

        self.awaitingCheckBox = QCheckBox("Awaiting:", self.timeGroupBox)
        timeLayout.addWidget(self.awaitingCheckBox, 2, 1)
        self.awaitingSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.awaitingSpinBox, 2, 2)
        self.awaitingComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.awaitingComboBox, 2, 3)

        self.cookingCheckBox = QCheckBox("Cooking:", self.timeGroupBox)
        timeLayout.addWidget(self.cookingCheckBox, 3, 1)
        self.cookingSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.cookingSpinBox, 3, 2)
        self.cookingComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.cookingComboBox, 3, 3)

        self.totalTimeCheckBox = QCheckBox("Total time:", self.timeGroupBox)
        timeLayout.addWidget(self.totalTimeCheckBox, 4, 1)
        self.totalTimeSpinBox = QDoubleSpinBox(self.timeGroupBox)
        timeLayout.addWidget(self.totalTimeSpinBox, 4, 2)
        self.totalTimeComboBox = QComboBox(self.timeGroupBox)
        timeLayout.addWidget(self.totalTimeComboBox, 4, 3)

        self.timeGroupBox.setLayout(timeLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Ingredients group box in filters pane: ———————————————————————————————
        cornucopiaLayout = QGridLayout()

        self.atLeastRadioButton = QRadioButton("At least…",
                                               self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.atLeastRadioButton, 1, 1)
        self.atMostRadioButton = QRadioButton("At most…",
                                              self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.atMostRadioButton, 1, 2)
        # self.ingredientsListView = QListWidget(self.cornucopiaGroupBox)
        self.foodstuffTableWidget = QTableWidget(0, 2, self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.foodstuffTableWidget, 2, 1, 7, 2)
        self.vegetarianCheckBox = QCheckBox("Vegetarian only",
                                            self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.vegetarianCheckBox, 2, 3)
        self.vegetalianCheckBox = QCheckBox("Vegetalian only",
                                            self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.vegetalianCheckBox, 3, 3)
        self.seasonCheckBox = QCheckBox("Out of season",
                                            self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.seasonCheckBox, 4, 3)
        allergensGroupBox = QGroupBox("Exclude allergens:", self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(allergensGroupBox, 5, 3)
        self.addFoodstuffPushButton = QPushButton("Add…", self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.addFoodstuffPushButton, 6, 3)
        self.editFoodstuffPushButton = QPushButton("Edit…", self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.editFoodstuffPushButton, 7, 3)
        self.throwFoodstuffPushButton = QPushButton("Throw", self.cornucopiaGroupBox)
        cornucopiaLayout.addWidget(self.throwFoodstuffPushButton, 8, 3)

        self.cornucopiaGroupBox.setLayout(cornucopiaLayout)
        # ——————————————————————————————————————————————————————————————————————

        # Allergen group box in ingredients group box: —————————————————————————
        allergensLayout = QHBoxLayout()

        self.allergensListView = QListWidget(allergensGroupBox)
        allergensLayout.addWidget(self.allergensListView)

        allergensGroupBox.setLayout(allergensLayout)
        # ——————————————————————————————————————————————————————————————————————

    def _check_add_tab(self):
        """Add a new tab at the end of the recipe pane if the user selected the
        “+” tab."""
        index = self.recipesTabWidget.currentIndex()
        if self.recipesTabWidget.tabText(index) == "+":
            self._insert_recipe_tab()

    def _checked_foodstuff(self):
        for row in range(self.foodstuffTableWidget.rowCount()):
            if self.foodstuffTableWidget.item(row, 0).checkState():
                yield self.foodstuffTableWidget.item(row, 1).text()

    def _clicked_add_foodstuff(self):
        """Launch the foodstuff edition window if the user clicked on the add
           foodstuff button."""
        self.open_edit_foodstuff(Foodstuff())

    def _clicked_add_recipe(self):
        """Launch the recipe edition window if the user clicked on the add
           recipe button."""
        self.open_edit_recipe(None)

    def _clicked_edit_foodstuff(self):
        """Launch the foodstuff edition window if the user clicked on the edit
           foodstuff button."""
        if len(self.foodstuffTableWidget.selectedItems()) > 0:
            foodstuff = self.foodstuffTableWidget.selectedItems()[0].text()
            self.open_edit_foodstuff(self.cb.cornucopia[foodstuff])

    def _clicked_edit_recipe(self):
        """Launch the recipe edition window if the user clicked on the edit
           recipe button."""
        if len(self.recipesListView.selectedItems()) > 0:
            row = self.recipesListView.selectedIndexes()[0].row()
            self.open_edit_recipe(row)

    def _clicked_new_cookbook(self):
        if not self.cb.hasBeenSaved:
            msgBox = QMessageBox(self)
            msgBox.setWindowTitle("Do you want to save before closing ?")
            msgBox.setText("The current cookbook has been edited. Do you want "
                           "to save before closing ?")
            msgBox.setIcon(msgBox.Question)
            msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No |
                                      QMessageBox.Cancel)
            button = msgBox.exec()
            if button == QMessageBox.Cancel:
                return
            elif button == QMessageBox.Yes:
                self.save()
                if not self.cb.hasBeenSaved:
                    return
        self.cb = CookBook()
        self.cbCache = CookBook(self.cb)
        self.cbHistory = deque([], maxlen=15)
        self.cbRHistory = deque([], maxlen=15)
        self._update_recipes_list()
        self._update_cornucopia()
        self._update_allergens()
        self._update_filters()
        self.setWindowTitle("KookbooC")
        self.cb.hasBeenSaved = True

    def _clicked_redo(self):
        self.cbHistory.append(CookBook(self.cb))
        self.undoAction.setEnabled(True)
        self.cb = CookBook(self.cbRHistory.pop())
        if len(self.cbRHistory) == 0:
            self.redoAction.setDisabled(True)
        self.cbCache = CookBook(self.cb)
        self._update_recipes_list()
        self._update_cornucopia()
        self._update_allergens()
        self._update_filters()
        self.setWindowTitle("KookbooC*")
        self.cb.hasBeenSaved = False

    def _clicked_undo(self):
        self.cbRHistory.append(CookBook(self.cb))
        self.redoAction.setEnabled(True)
        self.cb = CookBook(self.cbHistory.pop())
        if len(self.cbHistory) == 0:
            self.undoAction.setDisabled(True)
        self.cbCache = CookBook(self.cb)
        self._update_recipes_list()
        self._update_cornucopia()
        self._update_allergens()
        self._update_filters()
        self.setWindowTitle("KookbooC*")
        self.cb.hasBeenSaved = False

    def _clicked_throw_foodstuff(self):
        """If the user clicked on the throw foodstuff button, check if the
           foodstuff is not used and delete it."""
        def contains(i, foodstuff):
            while foodstuff in self.cb.recipes[i].ingredients:
                msgBox = QMessageBox(
                    QMessageBox.Question, "Do not waste good food !",
                    "You may need {} to cook {}. Do you want to modify this"
                    " recipe?".format(foodstuff,
                                      self.cb.recipes[i].name),
                    QMessageBox.Yes | QMessageBox.No, self)
                if msgBox.exec() == QMessageBox.Yes:
                    self.open_edit_recipe(i)
                else:
                    return True
            return False

        def is_used(foodstuff):
            for i in range(len(self.cb.recipes)):
                if contains(i, foodstuff):
                    return True
            return False

        for foodstuffItem in self.foodstuffTableWidget.selectedItems():
            if not is_used(foodstuffItem.text()):
                self.cb.cornucopia.pop(foodstuffItem.text())
                self._set_unsaved()
        self._update_cornucopia()
        self._update_allergens()

    def _close_tab(self, index):
        """
        Close a tab in the recipe pane.
        :param index: index of the tab to close.
        """
        if self.recipesTabWidget.tabText(index + 1) == "+" and index >= 1:
            self.recipesTabWidget.setCurrentIndex(index - 1)
        self.recipesTabWidget.removeTab(index)

    def _config_widgets(self):
        """Set the initial state of all the widgets of the window."""
        # Set filters pane size:
        self.setToolTipDuration(3000)
        self.newAction.statusTip()
        self.newAction.setToolTip("Have you just left mummy's house?")
        self.openAction.setToolTip("Time to humiliate Marco Ferreri himself!")
        self.undoAction.setDisabled(True)
        self.redoAction.setDisabled(True)
        self.saveAction.setToolTip("You should be able to understand the "
                                   "purpose of this button without a tooltip.")
        self.saveAsAction.setToolTip("Don't override the work of a life!")
        self.importCookbookAction.setToolTip("Does your grandma use this soft?")

        self.filtersGroupBox.setSizePolicy(QSizePolicy.Fixed,
                                           QSizePolicy.Expanding)
        w = self.atLeastRadioButton.sizeHint().width() +\
            self.atMostRadioButton.sizeHint().width()
        self.foodstuffTableWidget.setMaximumWidth(w)
        self.allergensListView.setMaximumWidth(w)
        self.allergensListView.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # Set filters state:
        for child in self.typeGroupBox.findChildren(QCheckBox):
            child.setChecked(True)

        self.masteredCheckBox.setChecked(False)

        for child in self.timeGroupBox.findChildren(QComboBox):
            child.addItems(
                ("s.", "min.", "h.", "days", "weeks", "months", "years"))
            child.setCurrentIndex(1)

        self.foodstuffTableWidget.setSelectionMode(
            QAbstractItemView.ExtendedSelection)
        self.foodstuffTableWidget.verticalHeader().hide()
        self.foodstuffTableWidget.horizontalHeader().hide()
        self.foodstuffTableWidget.setColumnWidth(0, 15)
        self.foodstuffTableWidget.horizontalHeader().setStretchLastSection(True)
        self.foodstuffTableWidget.setShowGrid(False)

 #       for ingredient in self.cb.recipe.ingredients.values():
  #          self._add_line(ingredient) # < -----------------------------------------------------
#        self.ingredientsTableWidget.setSizePolicy(QSizePolicy.Minimum,
 #                                                 QSizePolicy.MinimumExpanding)

        self.vegetarianCheckBox.setChecked(True)
        self.atMostRadioButton.setChecked(True)

        # Set recipes list pane size:
        self.recipesWidget.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)

        # Set recipes list pane state:
        self.searchBarLineEdit.setClearButtonEnabled(True)
        self.searchBarLineEdit.setPlaceholderText("Search recipe…")

        # Set recipe view pane state:
        self.recipesTabWidget.setTabsClosable(True)
        self.recipesTabWidget.addTab(QWidget(), "+")
        self.recipesTabWidget.tabBar().setTabButton(0, QTabBar.RightSide, None)
        self._insert_recipe_tab()
        self.guestsLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.guestsSpinBox.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.guestsSpinBox.setMinimum(1)
        self.guestsSpinBox.setValue(4)

    def _connect_signals_and_slots(self):
        """Connect signals and slots."""
        self.newAction.triggered.connect(self._clicked_new_cookbook)
        self.openAction.triggered.connect(self.load)
        self.undoAction.triggered.connect(self._clicked_undo)
        self.redoAction.triggered.connect(self._clicked_redo)
        self.saveAction.triggered.connect(self.save)
        self.saveAsAction.triggered.connect(self.save_as)

        self.masteredCheckBox.stateChanged.connect(self._update_filters)

        for child in self.typeGroupBox.findChildren(QCheckBox):
            child.toggled.connect(self._update_filters)

        for child in self.timeGroupBox.findChildren(QCheckBox):
            child.toggled.connect(self._update_filters)

        for child in self.timeGroupBox.findChildren(QDoubleSpinBox):
            child.valueChanged.connect(self._update_filters)

        for child in self.timeGroupBox.findChildren(QComboBox):
            child.currentTextChanged.connect(self._update_filters)

        self.vegetarianCheckBox.toggled.connect(self._update_filters)
        self.vegetalianCheckBox.toggled.connect(self._update_filters)
        self.seasonCheckBox.toggled.connect(self._update_filters)
        self.allergensListView.selectionModel().selectionChanged.connect(self._update_filters)

        self.atLeastRadioButton.toggled.connect(
            lambda: self._update_filters() if self.atLeastRadioButton.isChecked() else None)
        self.atMostRadioButton.toggled.connect(
            lambda: self._update_filters() if self.atMostRadioButton.isChecked() else None)

        self.foodstuffTableWidget.itemChanged.connect(self._update_filters)

        self.addFoodstuffPushButton.clicked.connect(self._clicked_add_foodstuff)
        self.editFoodstuffPushButton.clicked.connect(self._clicked_edit_foodstuff)
        self.throwFoodstuffPushButton.clicked.connect(self._clicked_throw_foodstuff)

        self.searchBarLineEdit.textChanged.connect(self._update_filters)
        self.recipesListView.selectionModel().selectionChanged.connect(self._update_current_tab)
        self.recipesListView.doubleClicked.connect(self._insert_recipe_tab)
        self.addRecipePushButton.clicked.connect(self._clicked_add_recipe)
        self.editRecipePushButton.clicked.connect(self._clicked_edit_recipe)

        self.recipesTabWidget.currentChanged.connect(self._check_add_tab)
        self.recipesTabWidget.tabCloseRequested.connect(self._close_tab)
        self.guestsSpinBox.valueChanged.connect(self._update_current_tab)

    def _filtered_cornucopia(self):
        for foodstuff in self.cb.cornucopia.values():
            if self.vegetarianCheckBox.isChecked() and not foodstuff.isVegetarian:
                continue
            if self.vegetalianCheckBox.isChecked() and not foodstuff.isVegetalian:
                continue
            season = Foodstuff.MONTHSLIST[datetime.today().month-1]
            if not self.seasonCheckBox.isChecked() and season not in foodstuff.seasons:
                continue
            for allergen in self.allergensListView.selectedItems():
                if allergen.text() in foodstuff.allergensSet:
                    break
            else:
                yield foodstuff.name

    def _is_filtered(self, recipe):
        # If masteredCheckBox is checked, filters not mastered recipes.
        if self.masteredCheckBox.isChecked() and not recipe.mastered:
            return True
        # Filter recipe containing filtered ingredients.
        if not recipe.needs_at_most(list(self._filtered_cornucopia())):
            return True
        # If atMostRadioButton is selected,
        # filter recipes containing unchecked ingredients.
        if self.atMostRadioButton.isChecked():
            if not recipe.needs_at_most(list(self._checked_foodstuff())):
                return True
        # if atLeastRadioButton is selected,
        # filter recipes not containing checked ingredients.
        elif self.atLeastRadioButton.isChecked():
            if not recipe.contains_at_least(list(self._checked_foodstuff())):
                return True
        # Filter recipes too long.
        if self.preparationCheckBox.isChecked():
            value = self.preparationSpinBox.value()
            unit = self.preparationComboBox.currentText()
            preparationTime = CustomTimeDelta(value=value, unit=unit)
            if recipe.time["preparation"] > preparationTime:
                return True

        if self.awaitingCheckBox.isChecked():
            value = self.awaitingSpinBox.value()
            unit = self.awaitingComboBox.currentText()
            awaitingTime = CustomTimeDelta(value=value, unit=unit)
            if recipe.time["awaiting"] > awaitingTime:
                return True

        if self.cookingCheckBox.isChecked():
            value = self.cookingSpinBox.value()
            unit = self.cookingComboBox.currentText()
            cookingTime = CustomTimeDelta(value=value, unit=unit)
            if recipe.time["cooking"] > cookingTime:
                return True

        if self.totalTimeCheckBox.isChecked():
            value = self.totalTimeSpinBox.value()
            unit = self.totalTimeComboBox.currentText()
            totalTime = CustomTimeDelta(value=value, unit=unit)
            if recipe.total_time() > totalTime:
                return True
        #
        for child, key in zip(self.typeGroupBox.findChildren(QCheckBox),
                              recipe.type):
            if child.isChecked() and recipe.type[key]:
                break
        else:
            return True

        regex = QRegularExpression(self.searchBarLineEdit.text())
        regex.setPatternOptions(QRegularExpression.CaseInsensitiveOption)
        if not regex.match(recipe.name).hasMatch():
            return True

        return False

    def _filtered_recipes(self):
        for recipe in self.cb.recipes:
            if self._is_filtered(recipe):
                continue

            yield recipe.name

    def _insert_recipe_tab(self):
        """Insert a new tab at the current index in the recipe pane."""
        index = self.recipesTabWidget.currentIndex()
        self.recipesTabWidget.insertTab(index, QTextBrowser(), "New recipe")
        self.recipesTabWidget.setCurrentIndex(index)
        if self._selected_recipe():
            self._update_current_tab()

    def _selected_recipe(self):
        if len(self.recipesListView.selectedItems()) > 0:
            i = self.recipesListView.selectedIndexes()[0].row()
            return self.cb.recipes[i]
        else:
            return None

    def _unchecked_foodstuff(self):
        for row in self.foodstuffTableWidget.rowCount():
            if not self.foodstuffTableWidget.item(row, 0).checkState():
                yield self.foodstuffTableWidget.item(row, 1).text()

    def _update_allergens(self):
        self.allergensListView.clear()
        allergens = self.cb.referenced_allergens()
        self.allergensListView.addItems(allergens)
        self.allergensListView.sortItems()

    def _update_cornucopia(self):
        with BlockedSignals(self.foodstuffTableWidget):
            self.foodstuffTableWidget.setRowCount(0)
            for row, foodstuffName in enumerate(sorted(self.cb.cornucopia)):
                foodstuffItem = QTableWidgetItem(foodstuffName)
                foodstuffItem.setFlags(foodstuffItem.flags() ^ Qt.ItemIsEditable)
                checkBoxItem = QTableWidgetItem()
                checkBoxItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
                checkBoxItem.setCheckState(Qt.Checked)
                if row >= self.foodstuffTableWidget.rowCount():
                    self.foodstuffTableWidget.insertRow(row)
                    self.foodstuffTableWidget.setRowHeight(row, 15)
                self.foodstuffTableWidget.setItem(row, 0, checkBoxItem)
                self.foodstuffTableWidget.setItem(row, 1, foodstuffItem)

    def _update_current_tab(self):
        index = self.recipesTabWidget.currentIndex()
        if recipe := self._selected_recipe():
            self.recipesTabWidget.setTabText(index, recipe.name)
            self.recipesTabWidget.widget(index).setMarkdown(
                self.to_markdown(recipe, self.guestsSpinBox.value()))

    def _update_filters(self):
        """Called each time a filter is modified by the user. Hide and display
        foodstuff and recipes with respect for filters settings."""
        with BlockedSignals(self.filtersGroupBox):
            # Filter foodstuff in the cornutopia QTableWidget with according to
            # the vegetarian/vegetalian, seasons and allergens selections.
            for row in range(self.foodstuffTableWidget.rowCount()):
                ingredientName = self.foodstuffTableWidget.item(row, 1).text()
                self.foodstuffTableWidget.setRowHidden(
                    row, ingredientName not in self._filtered_cornucopia())

            # Filter recipes in the recipes QListView.
            for row in range(self.recipesListView.model().rowCount()):
                item = self.recipesListView.item(row)
                recipe = self.cb.recipes[row]
                self.recipesListView.setItemHidden(item,
                                                   self._is_filtered(recipe))

    def _update_recipes_list(self):
        self.recipesListView.clear()
        self.recipesListView.addItems(
            (recipe.name for recipe in self.cb.recipes))

    def _set_unsaved(self):
        self.setWindowTitle("KookbooC*")
        self.cb.hasBeenSaved = False
        self.cbHistory.append(CookBook(self.cbCache))
        self.undoAction.setEnabled(True)
        self.cbRHistory = deque([], maxlen=15)
        self.cbCache = CookBook(self.cb)

    def closeEvent(self, event):
        """Override of QWidget method."""
        if not self.cb.hasBeenSaved:
            msgBox = QMessageBox(self)
            msgBox.setWindowTitle("Do you want to save before closing ?")
            msgBox.setText("The cookbook has been edited. Save the"
                           "cookbook before closing ?")
            msgBox.setIcon(msgBox.Question)
            msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No |
                                      QMessageBox.Cancel)
            button = msgBox.exec()
            if button == QMessageBox.Cancel:
                event.ignore()
            elif button == QMessageBox.Yes:
                self.save()
                if self.cb.hasBeenSaved:
                    event.accept()
                else:
                    event.ignore()
            elif button == QMessageBox.No:
                event.accept()

    def load(self):
        """Load a cookbook."""
        if not self.cb.hasBeenSaved:
            msgBox = QMessageBox(self)
            msgBox.setWindowTitle("Do you want to save before closing ?")
            msgBox.setText("The current cookbook has been edited. Do you want "
                           "to save before closing ?")
            msgBox.setIcon(msgBox.Question)
            msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No |
                                      QMessageBox.Cancel)
            button = msgBox.exec()
            if button == QMessageBox.Cancel:
                return
            elif button == QMessageBox.Yes:
                self.save()
                if not self.cb.hasBeenSaved:
                    return
        fileName = QFileDialog.getOpenFileName(self, "Open a cookbook",
                                               filter="Cookbook Files (*.xml)")[0]
        try:
            self.cb = CookBook.from_xml(ElementTree(file=fileName))
        except ValueError as err:
             msgBox= QMessageBox(self)
             msgBox.setWindowTitle("Impossible to load the cookbook.")
             msgBox.setText(
                 "Impossible to load the cookbook.\n"
                 "The file “{}” is not a valid cookbook file.".format(fileName))
             msgBox.setInformativeText(str(err))
             msgBox.setIcon(msgBox.Icon.Critical)
             msgBox.exec_()
        else:
            self.cb.file = fileName
            self.cbCache = CookBook(self.cb)
            self.cbHistory = deque([], maxlen=15)
            self.cbRHistory = deque([], maxlen=15)
            self._update_recipes_list()
            self._update_cornucopia()
            self._update_allergens()
            self._update_filters()
            self.setWindowTitle("KookbooC")
            self.cb.hasBeenSaved = True

    def open_edit_foodstuff(self, foodstuff=Foodstuff()):
        """
        Open foodstuff edition window and update cornucopia in consequence.
        :param foodstuff: the foodstuff to edit.
        :return: foodstuff object, or None if the user canceled.
        """
        editedFoodstuff = foodstuff.name
        foodstuff = EditFoodstuffWindow(self, foodstuff).run()
        if foodstuff:
            if editedFoodstuff != "":
                self.cb.cornucopia.pop(editedFoodstuff, None)
            try:
                self.cb.add_foodstuff(foodstuff)
            except ValueError as e:
                msgBox = QMessageBox(
                    QMessageBox.Question, "Do not waste old food !",
                    "You already have {} in your Cornucopia. Do you want to "
                    "throw and replace it?".format(editedFoodstuff),
                    QMessageBox.Ok | QMessageBox.Cancel, self)
                if msgBox.exec() == QMessageBox.Ok:
                    self.cb.cornucopia.pop(foodstuff.name, None)
                    self.cb.add_foodstuff(foodstuff)
                else:
                    self.open_edit_foodstuff(foodstuff)
            finally:
                self._update_cornucopia()
                self._update_allergens()
                self._update_filters()
                self._set_unsaved()

        return foodstuff

    def open_edit_recipe(self, i=None):
        """Called when the user click on “Edit…” recipe button.
        Just open the recipe edition window."""
        rec = self.cb.recipes[i] if i is not None else Recipe()
        editRecipeWindow = EditRecipeWindow(self, rec)
        if recipe := editRecipeWindow.run():
            if i is not None:
                self.cb.recipes[i] = recipe
                self.recipesListView.item(i).setText(recipe.name)
            else:
                self.cb.recipes.append(recipe)
                self.cb.recipes.sort(key=lambda recipe: recipe.name)
                self._update_recipes_list()
            self._update_current_tab()
            self._set_unsaved()

    def run(self, qt_app):
        """Show the application window and start the main event loop"""
        #qt_app.aboutToQuit.connect(self._confirmClosing)
        self.show()
        sys.exit(qt_app.exec_())

    def save(self):
        try:
            self.cb.save()
            self.setWindowTitle("KookbooC")
            self.cb.hasBeenSaved = True
        except ValueError:
            self.save_as()

    def save_as(self):
        file = QFileDialog.getSaveFileName(self, "Save as…",
                                           filter="Cookbook Files (*.xml)")[0]
        if file:
            file = Path(file)
            file = file.splitext()[0] + ".xml"
            self.cb.save_as(file)
            self.setWindowTitle("KookbooC")
            self.cb.hasBeenSaved = True

    def to_markdown(self, recipe, guestsNumber):
        title = "# {}\n".format(recipe.name)
        guests = " ## Ingredients for {} guest{}:\n".format(
            guestsNumber, "s" if guestsNumber > 1 else "")
        ingredients = str()
        for ingredient in recipe.ingredients.values():
            ingredientName = ingredient.name
            qt = round(ingredient.quantity * guestsNumber / recipe.guestsNumber,1)
            if ingredientName in (recipe.name.lower() for recipe in self.cb.recipes):
                ingredientName = "[{}]()".format(ingredientName)
            if not ingredient.opt:
                ingredients += "- {} : {} {} ;\n".format(
                    ingredientName, qt, ingredient.unit)
            else:
                ingredients += "- ({} : {} {}) ;\n".format(
                    ingredientName, qt, ingredient.unit)
        ingredients = ingredients.rstrip(" ;\n") + ".\n"
        instructions = "## Instructions:\n\n" + recipe.instructions
        return "\n\n".join((title, guests, ingredients, instructions))
