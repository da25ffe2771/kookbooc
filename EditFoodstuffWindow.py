from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout,\
    QGroupBox, QWidget, QListWidget, QPushButton, QGridLayout, QCheckBox,\
    QLineEdit, QCompleter
from Foodstuff import Foodstuff


class EditFoodstuffWindow(QDialog):
    """
    The window to edit foodstuff (The word “ingredient” is used for a foodstuff
    when it is used in a recipe).
    """
    def __init__(self, parent, foodstuff=Foodstuff()):
        """Constructor of foodstuff edition window."""
        QDialog.__init__(self, parent)
        self.setModal(True)
        self.setWindowTitle("Add ingredients")
        self.foodstuff = foodstuff
        self._build_window()
        self._config_widgets()
        self._connect_signals_and_slots()

    def _add_allergen(self):
        """Add a new allergen in the foodstuff if when “+“ push button is
           clicked."""
        if self.searchBarLineEdit.text() == "":
            return
        self.foodstuff.add_allergen(self.searchBarLineEdit.text())
        self._update_allergens_list()
        self.searchBarLineEdit.clear()

    def _build_window(self):
        """Called by the constructor to place all the elements of the window."""
        # Main window items:
        mainLayout = QVBoxLayout()

        self.nameLineEdit = QLineEdit(self)
        mainLayout.addWidget(self.nameLineEdit)
        self.vegetarianCheckBox = QCheckBox("Vegetarian", self)
        mainLayout.addWidget(self.vegetarianCheckBox)
        self.vegetalianCheckBox = QCheckBox("Vegetalian", self)
        mainLayout.addWidget(self.vegetalianCheckBox)
        self.seasonsGroupBox = QGroupBox("Seasons", self)
        mainLayout.addWidget(self.seasonsGroupBox)
        self.allergensGroupBox = QGroupBox("Allergens", self)
        mainLayout.addWidget(self.allergensGroupBox)
        self.dialogButtonBox = QDialogButtonBox(QDialogButtonBox.Ok |
                                                QDialogButtonBox.Cancel)
        mainLayout.addWidget(self.dialogButtonBox)

        self.setLayout(mainLayout)

        # Seasons GroupBox items:
        seasonsLayout = QGridLayout()
        seasonsLayout.setAlignment(Qt.AlignCenter)

        self.januaryCheckBox = QCheckBox("Jan.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.januaryCheckBox, 1, 3)
        self.februaryCheckBox = QCheckBox("Feb.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.februaryCheckBox, 1, 4)
        self.marchCheckBox = QCheckBox("Mar.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.marchCheckBox, 2, 4)
        self.aprilCheckBox = QCheckBox("Apr.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.aprilCheckBox, 3, 4)
        self.mayCheckBox = QCheckBox("May", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.mayCheckBox, 4, 4)
        self.juneCheckBox = QCheckBox("June", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.juneCheckBox, 4, 3)
        self.julyCheckBox = QCheckBox("July", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.julyCheckBox, 4, 2)
        self.augustCheckBox = QCheckBox("Aug.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.augustCheckBox, 4, 1)
        self.septemberCheckBox = QCheckBox("Sep.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.septemberCheckBox, 3, 1)
        self.octoberCheckBox = QCheckBox("Oct.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.octoberCheckBox, 2, 1)
        self.novemberCheckBox = QCheckBox("Nov.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.novemberCheckBox, 1, 1)
        self.decemberCheckBox = QCheckBox("Dec.", self.seasonsGroupBox)
        seasonsLayout.addWidget(self.decemberCheckBox, 1, 2)
        self.wholeYearWidget = QWidget(self.seasonsGroupBox)
        seasonsLayout.addWidget(self.wholeYearWidget, 2, 2, 2, 2)

        self.seasonsGroupBox.setLayout(seasonsLayout)

        wholeYearLayout = QVBoxLayout()
        wholeYearLayout.setAlignment(Qt.AlignCenter)

        self.wholeYearCheckBox = QCheckBox("Whole year", self.wholeYearWidget)
        wholeYearLayout.addWidget(self.wholeYearCheckBox)

        self.wholeYearWidget.setLayout(wholeYearLayout)

        # Allergens Group Box:
        allergensLayout = QGridLayout()

        self.searchBarLineEdit = QLineEdit(self.allergensGroupBox)
        allergensLayout.addWidget(self.searchBarLineEdit, 1, 1)
        self.addAllergenPushButton = QPushButton("+", self.allergensGroupBox)
        allergensLayout.addWidget(self.addAllergenPushButton, 1, 2)
        self.delAllergenPushButton = QPushButton("-", self.allergensGroupBox)
        allergensLayout.addWidget(self.delAllergenPushButton, 1, 3)
        self.allergensListWidget = QListWidget(self.allergensGroupBox)
        allergensLayout.addWidget(self.allergensListWidget, 2, 1, 1, 3)

        self.allergensGroupBox.setLayout(allergensLayout)

    def _config_widgets(self):
        self.nameLineEdit.setPlaceholderText("Ingredient name")
        self.nameLineEdit.setText(self.foodstuff.name)
        self.vegetarianCheckBox.setCheckState(
            Qt.CheckState.Checked if self.foodstuff.isVegetarian else Qt.CheckState.Unchecked)
        self.vegetalianCheckBox.setCheckState(
            Qt.CheckState.Checked if self.foodstuff.isVegetalian else Qt.CheckState.Unchecked)
        for checkbox in self.seasonsGroupBox.findChildren(QCheckBox):
            if checkbox.text() in self.foodstuff.seasons:
                checkbox.setCheckState(Qt.CheckState.Checked)
        self._update_whole_year_checkbox()
        self.searchBarLineEdit.setClearButtonEnabled(True)
        self.searchBarLineEdit.setPlaceholderText("Gluten, lactose…")
        completer = QCompleter(self.parent().cb.referenced_allergens())
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.searchBarLineEdit.setCompleter(completer)
        self.addAllergenPushButton.setFixedWidth(
            self.addAllergenPushButton.height())
        self.delAllergenPushButton.setFixedWidth(
            self.delAllergenPushButton.height())
        self._update_allergens_list()

    def _connect_signals_and_slots(self):
        self.nameLineEdit.editingFinished.connect(self._update_name)
        self.vegetarianCheckBox.toggled.connect(self._vegetarian_toggled)
        self.vegetalianCheckBox.toggled.connect(self._vegetalian_toggled)
        for checkbox in self.seasonsGroupBox.findChildren(QCheckBox):
            if checkbox.text() == "Whole year":
                checkbox.clicked.connect(self._whole_year_toggled)
            else:
                checkbox.clicked.connect(self._update_seasons)
        self.addAllergenPushButton.clicked.connect(self._add_allergen)
        self.delAllergenPushButton.clicked.connect(self._del_allergen)
        self.dialogButtonBox.accepted.connect(self.accept)
        self.dialogButtonBox.rejected.connect(self.reject)

    def _del_allergen(self):
        """Delete selected allergen in the foodstuff if when “-“ push button is clicked

        :return: None
        """
        listItems = self.allergensListWidget.selectedItems()
        if not listItems: return
        for item in listItems:
            self.foodstuff.allergensSet.remove(item.text())
        self._update_allergens_list()

    def _update_allergens_list(self):
        """Set allergensListWidget with the allergens contained by the foodstuff

        :return:
        """
        self.allergensListWidget.clear()
        self.allergensListWidget.addItems(self.foodstuff.allergensSet)

    def _update_name(self):
        self.foodstuff.name = self.nameLineEdit.text()

    def _update_seasons(self):
        """Update foodstuff.seasons when a season checkbox is toggled and set
        the state of the whole year checkbox

        :return:
        """
        self.foodstuff.seasons = set()
        for checkbox in self.seasonsGroupBox.findChildren(QCheckBox):
            if checkbox.checkState() == Qt.CheckState.Checked and checkbox.text() != "Whole year":
                self.foodstuff.seasons.add(checkbox.text())
        self._update_whole_year_checkbox()

    def _update_whole_year_checkbox(self):
        if len(self.foodstuff.seasons) == 0:
            self.wholeYearCheckBox.setTristate(False)
            self.wholeYearCheckBox.setCheckState(Qt.CheckState.Unchecked)
        elif len(self.foodstuff.seasons) == 12:
            self.wholeYearCheckBox.setTristate(False)
            self.wholeYearCheckBox.setCheckState(Qt.CheckState.Checked)
        else:
            self.wholeYearCheckBox.setCheckState(Qt.CheckState.PartiallyChecked)

    def _vegetarian_toggled(self):
        if self.vegetarianCheckBox.checkState() == Qt.CheckState.Unchecked:
            self.vegetalianCheckBox.setCheckState(Qt.CheckState.Unchecked)

        self.foodstuff.isVegetarian = bool(self.vegetarianCheckBox.checkState())
        self.foodstuff.isVegetalian = bool(self.vegetalianCheckBox.checkState())

    def _vegetalian_toggled(self):
        if self.vegetalianCheckBox.checkState() == Qt.CheckState.Checked:
            self.vegetarianCheckBox.setCheckState(Qt.CheckState.Checked)

        self.foodstuff.isVegetarian = bool(self.vegetarianCheckBox.checkState())
        self.foodstuff.isVegetalian = bool(self.vegetalianCheckBox.checkState())

    def _whole_year_toggled(self):
        state = self.wholeYearCheckBox.checkState()
        if state != Qt.CheckState.PartiallyChecked:
            self.wholeYearCheckBox.setTristate(False)
            for checkbox in self.seasonsGroupBox.findChildren(QCheckBox):
                    checkbox.setCheckState(state)
            self._update_seasons()

    def run(self):
        """Show the recipe edition window and start its event loop"""

        self.show()
        if self.exec_():
            return self.foodstuff
        else:
            return None
