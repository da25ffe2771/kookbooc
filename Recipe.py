from lxml import etree
from Ingredient import Ingredient
from CustomTimeDelta import CustomTimeDelta


class Recipe:
    def __init__(self, name=""):
        self.name = name
        self.mastered = False
        self.type = {"entree": False, "main_course": True, "dessert": False,
                     "aperitif": False, "snack": False, "drink": False}
        self.time = {"preparation": CustomTimeDelta(),
                     "awaiting": CustomTimeDelta(),
                     "cooking": CustomTimeDelta() }
        self.guestsNumber = 4
        self.ingredients = dict()
        self.instructions = str()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = str(name).capitalize()

    def add_ingredient(self, ingredient):
        if ingredient.name not in self.ingredients:
            self.ingredients[ingredient.name] = ingredient

    @classmethod
    def from_xml(cls, recipeElement, cornutopia):
        recipe = Recipe()
        recipe.name = recipeElement.get("name")
        recipe.mastered = recipeElement.get("mastered") == "true"
        for key, value in recipeElement.find("type").attrib.items():
            recipe.type[key] = (value == "true")
        recipe.instructions = recipeElement.find("instructions").text or ""

        for time in recipeElement.find("time"):
            recipe.time[time.tag] =\
                CustomTimeDelta(value=float(time.get("value")), unit=time.get("unit"))

        recipe.guestsNumber = int(recipeElement.find("ingredients").get("guests"))
        for ingredientElement in recipeElement.find("ingredients"):
            ingredient = Ingredient.from_xml(ingredientElement, cornutopia)
            recipe.add_ingredient(ingredient)

        return recipe

    def is_vegetalian(self):
        for ingredient in self.ingredients.values():
            if not ingredient.isVegetalian:
                return False
        return True

    def is_vegetarian(self):
        for ingredient in self.ingredients.values():
            if not ingredient.isVegetarian:
                return False
        return True

    def mandatory_ingredients(self):
        for ingredient in self.ingredients.values():
            if not ingredient.opt:
                yield ingredient

    def needs_at_most(self, ingredientsNames):
        """
        Return True if ingredientsName contains all the ingredients needed for
        the recipe, and False if the some mandatory ingredients are not in
        ingredientsNames.

        :param ingredientsNames: an iterable containing ingredients names.
        :return: bool
        """

        for ingredient in self.mandatory_ingredients():
            if ingredient.name not in ingredientsNames:
                return False
        return True

    def contains_at_least(self, ingredientsNames):
        """
        Return True if the recipe can contain all the ingredients in
        ingredientsName, and False if some of ingredients from ingredientsNames
        can not be used in the recipe.

        :param ingredientsNames: an iterable containing ingredients names.
        :return: bool
        """
        for ingredientName in ingredientsNames:
            if ingredientName not in self.ingredients:
                return False
        else:
            return True

    def optional_ingredients(self):
        for ingredient in self.ingredients.values():
            if ingredient.opt:
                yield ingredient

    def replace_ingredient(self, key, ingredient):
        self.ingredients.pop(key)
        self.add_ingredient(ingredient)

    def to_xml(self):
        recipeElement = etree.Element("recipe",
            {"name": self.name, "mastered": str(self.mastered).lower()})
        etree.SubElement(recipeElement, "type", {k: str(v).lower() for k, v in self.type.items()})

        timeElement = etree.SubElement(recipeElement, "time")
        for key, value in self.time.items():
            time = {"value": str(value.value).lower(), "unit": value.unit}
            etree.SubElement(timeElement, key, time)

        ingredientsElement = etree.SubElement(recipeElement, "ingredients")
        ingredientsElement.set("guests", str(self.guestsNumber))
        for ingredient in self.ingredients.values():
            ingredientsElement.append(ingredient.to_xml())

        etree.SubElement(recipeElement, "instructions").text = self.instructions

        return recipeElement

    def total_time(self):
        """Compute the sun of preparation, awaiting and cooking time.

        :return: CustomTimeDelta object
        """
        return sum(self.time.values(), start=CustomTimeDelta())



