
class BlockedSignals:
    """
    Context manager to temporarily block signals from Qt widgets.

    :param widgets: widgets on which signals will be blocked.
    """
    def __init__(self, *widgets):
        self.widgets = widgets

    def __enter__(self):
        for widget in self.widgets:
            widget.blockSignals(True)

    def __exit__(self, type, value, traceback):
        for widget in self.widgets:
            widget.blockSignals(False)