from lxml import etree
from path import Path
from itertools import chain
from functools import wraps

from Recipe import Recipe
from Foodstuff import Foodstuff
from Ingredient import Ingredient


class CookBook:
    """
    The cookbook is the main object used by the program. It contains all recipes
    and the cornucopia (the list of all foodstuff available), and can parse
    itself to xml for saving.
    """

    XMLSCHEMA = etree.XMLSchema(file="./CookBookSchema.xsd")

    def __init__(self, cookbook=None):
        """
        Initialise CookBook.
        :param cookbook: CookBook object to copy recipes and ingredients from.
        """
        self.recipes = list()           # Recipes list, referenced by their indexes.
        self.cornucopia = dict()        # List of all available ingredients(=foodstuff), referenced by their names.
        self.file = None                # Path of the xml containing the cookbook.
        self.hasBeenSaved = False    # To show the “*” in the window title, like in notepadd++.
        if cookbook is not None:
            self.recipes = list(cookbook.recipes)
            self.cornucopia = cookbook.cornucopia.copy()
            self.file = cookbook.file

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file):
        self._file = Path(file) if file else None

    def add_foodstuff(self, foodstuff):
        """Add a new ingredient to self.ingredient attribute.
        :param foodstuff: Ingredient object to add.
        """
        if foodstuff.name in self.cornucopia.keys():
            raise ValueError("Foodstuff {} is already in your "
                             "cornucopia".format(foodstuff.name))
        self.cornucopia[foodstuff.name] = foodstuff

    def delete_ingredient(self, foodstuffName):
        """Delete ingredient from self.ingredients.
        :param foodstuffName: name of the ingredient to delete.
        """
        del self.cornucopia[foodstuffName]

    @classmethod
    def from_xml(cls, root: etree.ElementTree):
        """
        Set recipes and ingredients from xml data.
        :param root: see etree.ElementTree() arguments
        :return: a CookBook object.
        """
        if not CookBook.XMLSCHEMA.validate(root):
            raise ValueError("The parameter doesn't match the xml schema.\n"
                             "{}".format(CookBook.XMLSCHEMA.error_log))

        cookbook = CookBook()

        for foodstuffElement in root.findall("/cornucopia/foodstuff"):
            cookbook.add_foodstuff(Foodstuff.from_xml(foodstuffElement))

        for recipeElement in root.findall("/recipes/recipe"):
            cookbook.recipes.append(Recipe.from_xml(recipeElement,
                                                    cookbook.cornucopia))
        return cookbook

    def merge(self, cookbook):
        """Add recipes and ingredients from another cookbook to the current
           cookbook. Duplicates ingredients are not added.
        :param cookbook: CookBook object.
        :return: self
        """
        self.recipes += cookbook.recipes
        self.cornucopia += {key: value for key, value in
                            cookbook.cornucopia.items()
                            if key not in self.cornucopia.keys()}
        return self

    def save(self):
        """Write the xml file if self.file is defined and if self.et match the
           cookbook xml schema.
        """
        self.save_as(self.file)


    def save_as(self, file):
        if file == "" or file is None:
            raise ValueError("Impossible to save file to the path “{}”".format(file))
        self.to_xml().write(file, encoding="utf8", xml_declaration=True,
                            pretty_print=True)
        self.file = file

    def to_xml(self):
        """Convert the cookbook into xml data matching with the XMLSCHEMA.
        :return: etree.ElementTree()
        """
        root = etree.Element("cookbook")
        recipesElement = etree.SubElement(root, "recipes")
        cornucopiaElement = etree.SubElement(root, "cornucopia")

        for recipe in self.recipes:
            recipesElement.append(recipe.to_xml())

        for foodstuff in self.cornucopia.values():
            cornucopiaElement.append(foodstuff.to_xml())

        elementTree = etree.ElementTree(root)
        assert CookBook.XMLSCHEMA.validate(elementTree)
        return elementTree

    def recipes_names_iterator(self):
        for recipe in self._recipes:
            yield recipe.name

    def ingredients_name_iterator(self):
        for ingredient in self._ingredients:
            yield ingredient.name

    def referenced_allergens(self):
        #allergens = set()
        #allergens.update(*(foodstuff.allergensSet for foodstuff in self.cornucopia.values()))
        #print(allergens)
        allergens = set()
        for foodstuff in self.cornucopia.values():
            allergens.update(foodstuff.allergensSet)
        return allergens
